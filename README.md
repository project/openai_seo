# OpenAI SEO Analyzer

The OpenAI SEO Analyzer is a Drupal module that integrates with the OpenAI GPT-4
Turbo model to provide SEO analysis directly within the node view. It allows
users to generate and customize SEO reports using AI-driven insights, storing
all results in the database for easy access and reference. This module is a
practical tool for site administrators and content managers looking to
enhance their content's SEO through advanced, AI-powered analytics.

## Additional requirements

This module is a part of [OpenAI](https://www.drupal.org/project/openai)
ecosystem and it is a required module for this to work.

## Post-installation

- Add your OpenAI API key to the settings of the OpenAI module
- Set correct permissions at /admin/people/permissions/module/openai_seo
- After that you can generate reports from the node views using SEO Analyzer tab
