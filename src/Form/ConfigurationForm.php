<?php

namespace Drupal\openai_seo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openai_seo\OpenAiAnalyzer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OpenAI SEO configuration form.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * OpenAI analyzer.
   *
   * @var \Drupal\openai_seo\OpenAiAnalyzer
   */
  protected $analyzer;

  /**
   * Constructs a new ConfigurationForm object.
   *
   * @param \Drupal\openai_seo\OpenAiAnalyzer $analyzer
   *   OpenAI analyzer.
   */
  public function __construct(OpenAiAnalyzer $analyzer) {
    $this->analyzer = $analyzer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_seo.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openai_seo.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_seo_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $preferences_token = NULL) {
    // Create the form.
    $config = $this->config('openai_seo.configuration');

    $form['prompt'] = [
      '#type' => 'details',
      '#title' => $this->t('Prompts'),
      '#open' => TRUE,
    ];

    $form['prompt']['header'] = [
      '#type' => 'markup',
      '#markup' => '<p>The default prompt comes with the module and it is the one that is used unless a custom prompt is provided below.</p>'
    ];

    $form['prompt']['default_prompt'] = [
      '#type' => 'textarea',
      '#readonly' => TRUE,
      '#disabled' => TRUE,
      '#title' => $this->t('Default prompt'),
      '#value' => $this->analyzer->getDefaultPrompt(),
    ];

    $form['prompt']['custom_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom prompt'),
      '#default_value' => $config->get('custom_prompt'),
    ];

    $form['advanced_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced configuration'),
      '#open' => TRUE,
    ];

    $form['advanced_config']['default_seed'] = [
      '#type' => 'number',
      '#title' => 'Default seed',
      '#description' => $this->t('If specified, a request with the same seed and parameters should return the same result. Use to generate reports in similar format. Read more at <a href="https://platform.openai.com/docs/api-reference/completions/create#completions-create-seed" target="_blank">OpenAI API reference</a>.'),
      '#required' => FALSE,
      '#default_value' => $config->get('default_seed'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, string $preferences_token = NULL) {
    $config = $this->config('openai_seo.configuration');
    $custom_prompt = $form_state->getValue('custom_prompt') ?? '';
    $config->set('custom_prompt', trim($custom_prompt))->save();
    $default_seed = $form_state->getValue('default_seed') ?? NULL;
    $config->set('default_seed', $default_seed)->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Page title.
   */
  public function getTitle() {
    return $this->t('Administer OpenAI SEO analyzer settings');
  }

}
